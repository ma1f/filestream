﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace File.Api.Controllers {
    [Route("api")]
    public class DownloadController : Controller {

        readonly static string baseDir = Path.Combine(AppContext.BaseDirectory, "resources");
        readonly static (string,string)[] _files = new (string,string)[] {
            ("xs.png", "image/png"),
            ("sm.jpg", "image/jpeg"),
            ("md.jpg", "image/jpeg"),
            ("lg.jpg", "image/jpeg"),
            ("xl.exe", "application/octet-stream"),
            ("xxl.exe", "application/octet-stream")
        };

        static string[] _paths;
        static string[] Paths => _paths ?? (_paths = _files.Select(x => Path.Combine(baseDir, x.Item1)).ToArray());
        static string[] ContentTypes => _files.Select(x => x.Item2).ToArray();

        static byte[][] _bytes;
        static byte[][] Bytes => _bytes ?? (_bytes = Paths.Select(x => System.IO.File.ReadAllBytes(x)).ToArray());
        
        [HttpGet("ping")]
        public IActionResult Ping() {
            return Ok();
        }

        [HttpGet("base64/{index}")]
        public async Task<string> Base64([FromRoute] int index) {
            return Convert.ToBase64String(await System.IO.File.ReadAllBytesAsync(Paths[index]));
        }

        [HttpGet("base64/{index}/memory")]
        public string Base64mem([FromRoute] int index) {
            return Convert.ToBase64String(Bytes[index]);
        }

        [HttpGet("file/{index}")]
        public IActionResult Download([FromRoute] int index) {
            return File(Path.Combine("resources", _files[index].Item1), ContentTypes[index]);
        }

        [HttpGet("file/{index}/memory")]
        public IActionResult DownloadMem([FromRoute] int index) {
            return File(Bytes[index], ContentTypes[index]);
        }

        [HttpGet("file/{index}/filestream")]
        public IActionResult DownloadStream([FromRoute] int index) {
            var fileStream = new FileStream(Paths[index], FileMode.Open, FileAccess.Read, FileShare.Read, 1024 * 64);
            return File(fileStream, ContentTypes[index]);
        }

        [HttpGet("filestream/{index}")]
        public IActionResult Stream([FromRoute] int index) {
            var fileStream = new FileStream(Paths[index], FileMode.Open, FileAccess.Read, FileShare.Read, 1024 * 64);
            return new FileStreamResult(fileStream, ContentTypes[index]);
        }

        [HttpGet("filestream/{index}/memory")]
        public IActionResult StreamMem([FromRoute] int index) {
            var memStream = new MemoryStream(Bytes[index]);
            return new FileStreamResult(memStream, ContentTypes[index]);
        }

    }
}
