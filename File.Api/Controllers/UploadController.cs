﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using UploadStream;

namespace File.Api.Controllers {

    [Route("api")]
    [Produces("application/json")]
    public class UploadController : Controller {
        
        readonly string _dir;
        const int BUF_SIZE = 4096;

        public enum Storage {
            None,
            MemoryStream,
            BufferedStream,
            File
        }

        public class Base64model {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Base64 { get; set; }
        }

        public class UploadModel {
            public string Name { get; set; }
            public string Description { get; set; }
            public List<IFormFile> Files { get; set; }
        }

        class StreamModel {
            public string Name { get; set; }
            public string Description { get; set; }
        }

        public UploadController() {
            _dir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
            if (!Directory.Exists(_dir))
                Directory.CreateDirectory(_dir);
        }

        [HttpPost("base64")]
        public IActionResult Base64([FromBody] Base64model model) {
            var bytes = Convert.FromBase64String(model.Base64);
            
            return Ok(new { model.Name, model.Description, Count = bytes.Length });
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(UploadModel model) {
            byte[] buffer = new byte[BUF_SIZE];
            
            foreach (var s in model.Files)
                using (var stream = s.OpenReadStream())
                    while (await stream.ReadAsync(buffer, 0, buffer.Length) > 0) ;
            
            return Ok(new { model.Name, model.Description, model.Files.Count });
        }

        [HttpPost("stream")]
        [DisableModelBinding]
        public async Task<IActionResult> ControllerModelStream() {
            var count = 0;
            byte[] buffer = new byte[BUF_SIZE];
            
            var model = await this.StreamFiles<StreamModel>(async x => {
                using (var stream = x.OpenReadStream())
                    while (await stream.ReadAsync(buffer, 0, buffer.Length) > 0) ;
                count++;
            });

            return Ok(new { model.Name, model.Description, Count = count });
        }

    }

}
