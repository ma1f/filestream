using System;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using FluentAssertions;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using Jil;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace UploadStream.UnitTests {
    public class StreamFiles {

        Mock<Controller> _controller = new Mock<Controller>().SetupAllProperties();

        [Fact]
        public async void ShouldStreamFile() {
            // arrange
            var request = new Mock<HttpRequest>().SetupAllProperties();
            _controller.SetupProperty(x => x.Request, request.Object);

            // action
            await ControllerExtensions.StreamFiles<Model>(_controller.Object, (formfile) => {

                // assert
                formfile.ContentType.Should().Be("image/png");
                formfile.Name.Should().Be("test");
            });
        }

        [Fact]
        public async void ShouldReturnModel() {
            // arrange
            var body = new StringContent(JSON.Serialize(new { Id = 1, Name = "name" }), Encoding.UTF8, "application/json");
            _controller.Setup(x => x.Request.Body).Returns(await body.ReadAsStreamAsync());

            // action
            var model = await ControllerExtensions.StreamFiles<Model>(_controller.Object, (formfile) => { });

            // assert
            model.Id.Should().Be(1);
            model.Name.Should().Be("name");
        }

        [Fact]
        public async void InvalidModelShouldReturnException() {
            // arrange
            var body = new StringContent(JSON.Serialize(new { Id = 1, Name = "name" }), Encoding.UTF8, "application/json");
            _controller.Setup(x => x.Request.Body).Returns(await body.ReadAsStreamAsync());

            // action
            var model = await ControllerExtensions.StreamFiles<Model>(_controller.Object, (formfile) => { });

            // assert
            
            model.Id.Should().Be(1);
            model.Name.Should().Be("name");
        }

    }

    class Model {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
    
}


