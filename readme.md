UploadStream - high performance file upload streaming for dotnet
=================================================================
[![pipeline status](https://gitlab.com/ma1f/uploadstream/badges/master/pipeline.svg)](https://gitlab.com/ma1f/uploadstream/commits/master)

Installation
------------
Nuget: [https://www.nuget.org/packages/UploadStream](https://www.nuget.org/packages/UploadStream)
```bash
> install-package UploadStream
```

Features
--------
Optimise file upload streaming performance, reduced CPU usage ~25%, reduced Memory usage ~50%.

Typical dotnet model binding loads the entire stream into `IEnumerable<IFormFile>` - this is non-ideal for large files
where processing of the stream should occur during upload rather then buffering the entire file(s) to memory/disk.
This package provides a `StreamFiles<T>(Action<IFormFile> func)` method that returns strongly typed model with `ModelState`
validation (default model binding is disabled via a custom `[DisableModelBinding]` attribute), while asynchronously streaming
uploads to be processed by the delegate function.

Usage
-----
```csharp
[HttpPost("upload")]
// disable default model binding in order to enable processing of streams without buffering entire stream
[DisableModelBinding]
public async Task<IActionResult> Upload() {
    MyModel model = await this.StreamFiles<MyModel>(async formFile => {
        // processing of stream as an IFormFile implementation
        using (var stream = formfile.OpenReadStream())
            await ...
    });
    // ModelState is validated despite disabling of default model binding
    if(!ModelState.IsValid)
        ...
}
```

Performance (Benchmark.Net)
-----------

### Results
Overall results are scaled in comparison to the default dotnet `IFormFile` model binding, the `UploadStream` package offers
performance improvements which appear to converge at ~25% reduced CPU usage, and ~50% reduced memory garbage collections for large files (20MB+),
while still offering performance improvements of ~20% reduced CPU usage and 45% reduced memory garbage collections for typical photo sizes (~6MB).

Out of interest a comparison to file uploads via a base64 json model was performed, interestingly this actually offers improved performance
for very small file sizes (<10kb), however becames extremely unwieldy at anything over the streaming buffer size (64kb or 80kb).
The base64 model will result in 30-40% increased bandwidth usage in addition to the increased CPU and Memory usage.

| Alias | File sizes | StreamFiles (us/gc) | Base64 (us) |
|------ |-----------:|--------------------:|------------:|
|    Xs |    5.94 KB |       1.02x / 0.97x |       0.75x |
|    Sm |  106.53 KB |       0.98x / 0.92x |       1.55x |
|    Md |  865.37 KB |       0.92x / 0.71x |       4.76x |
|    Lg |    6.04 MB |       0.79x / 0.65x |       8.90x |
|    Xl |   21.91 MB |       0.76x / 0.50x |      10.64x |
|   Xxl |  124.26 MB |       0.74x / 0.49x |      14.08x |

### Upload Performance
Detailed `Benchmark.Net` results for file uploads, comparing `base64`, `IFormFile` and `StreamFiles` for a range of file sizes.

|          Method |           Mean |         Error |        StdDev |         Median |   Scaled | ScaledSD |     Gen 0 |    Gen 1 |    Gen 2 | Allocated |
|---------------- |---------------:|--------------:|--------------:|---------------:|---------:|---------:|----------:|---------:|---------:|----------:|
|  UploadBase64Xs |       356.1 us |      6.316 us |      5.908 us |       358.0 us |     1.05 |     0.02 |   13.6719 |        - |        - |   6.61 KB |
|  UploadBase64Sm |     1,871.9 us |      5.862 us |      4.895 us |     1,870.7 us |     5.50 |     0.04 |    5.8594 |        - |        - |   6.61 KB |
|  UploadBase64Md |    13,184.3 us |    279.861 us |    435.710 us |    12,944.4 us |    38.74 |     1.28 |         - |        - |        - |   6.61 KB |
|  UploadBase64Lg |   103,441.8 us |  2,049.784 us |  3,643.490 us |   103,366.6 us |   303.94 |    10.73 |         - |        - |        - |   6.62 KB |
|  UploadBase64Xl |   364,726.7 us |  6,963.575 us |  6,839.161 us |   361,196.7 us | 1,071.67 |    20.54 |         - |        - |        - |   6.62 KB |
| UploadBase64Xxl | 2,382,051.0 us | 20,107.001 us | 17,824.331 us | 2,377,794.1 us | 6,999.12 |    66.23 |  312.5000 | 312.5000 | 312.5000 |   6.62 KB |
|    UploadFileXs |       340.3 us |      2.441 us |      2.164 us |       340.7 us |     1.00 |     0.00 |   17.0898 |        - |        - |   6.14 KB |
|    UploadFileSm |       389.3 us |      4.371 us |      3.875 us |       388.1 us |     1.14 |     0.01 |   19.5313 |        - |        - |   6.14 KB |
|    UploadFileMd |       754.1 us |     12.028 us |     11.251 us |       751.9 us |     2.22 |     0.03 |   36.1328 |        - |        - |   6.14 KB |
|    UploadFileLg |     3,182.0 us |     26.721 us |     22.313 us |     3,178.6 us |     9.35 |     0.09 |  156.2500 |        - |        - |   6.14 KB |
|    UploadFileXl |    10,635.9 us |     68.334 us |     63.920 us |    10,628.9 us |    31.25 |     0.26 |  531.2500 |        - |        - |   6.14 KB |
|   UploadFileXxl |    59,474.6 us |  1,184.690 us |  1,364.291 us |    58,887.0 us |   174.75 |     4.05 | 2937.5000 |        - |        - |   6.14 KB |
|  UploadStreamXs |       305.5 us |      1.423 us |      1.261 us |       305.1 us |     0.90 |     0.01 |   16.1133 |        - |        - |   6.14 KB |
|  UploadStreamSm |       350.2 us |      4.202 us |      3.930 us |       350.1 us |     1.03 |     0.01 |   17.5781 |        - |        - |   6.14 KB |
|  UploadStreamMd |       662.6 us |      6.182 us |      5.480 us |       662.1 us |     1.95 |     0.02 |   26.3672 |        - |        - |   6.14 KB |
|  UploadStreamLg |     2,537.8 us |     48.838 us |     56.242 us |     2,523.0 us |     7.46 |     0.17 |   85.9375 |        - |        - |   6.14 KB |
|  UploadStreamXl |     8,428.8 us |    162.618 us |    159.712 us |     8,386.3 us |    24.77 |     0.48 |  265.6250 |        - |        - |   6.14 KB |
| UploadStreamXxl |    45,302.5 us |    296.289 us |    247.415 us |    45,257.6 us |   133.11 |     1.07 | 1437.5000 |        - |        - |   6.14 KB |

#### Key
* UploadBase64* - default json model binding, convert from base64 to byte[]
* UploadFile* - default `IFormFile` model binding
* UploadStream* - custom `StreamFiles` stream processing and model binding


### Load Performance
Detailed `Benchmark.Net` results for file uploads with a 'load test' of 20 files.

Compares `base64`, `IFormFile` and `StreamFiles` for a range of file sizes, as well as comparing the performance difference
between 20x file uploads vs 1x file upload with 20x files (multi).

Results below are compared to `IFormFile` multi (1x upload, 20x files) as a baseline.

| Alias | File sizes | IFormFile 20x (us/gc) | StreamFiles 20x (us/gc) | StreamFiles Multi (us/gc) | Base64 20x (us) |
|------ |-----------:|----------------------:|------------------------:|--------------------------:|----------------:|
|    Xs |     5.94kB |         5.02x / 6.58x |           4.80x / 6.58x |             0.96x / 1.04x |           4.86x |
|    Sm |   106.53kB |         5.35x / 5.65x |           4.20x / 5.02x |             0.95x / 1.00x |          15.99x |
|    Md |   865.37kB |         8.07x / 9.88x |           5.53x / 7.53x |             0.91x / 0.88x |          77.65x |
|    Lg |     6.04MB |        9.25x / 16.00x |           6.18x / 9.17x |             0.80x / 0.63x |         234.21x |
|    Xl |    21.91MB |        8.03x / 18.67x |           5.92x / 9.67x |             0.78x / 0.53x |         254.96x |


|                  Method |           Mean |         Error |         StdDev |         Median |   Scaled | ScaledSD |      Gen 0 |    Gen 1 |    Gen 2 | Allocated |
|------------------------ |---------------:|--------------:|---------------:|---------------:|---------:|---------:|-----------:|---------:|---------:|----------:|
|      UploadBase64LoadXs |     3,524.8 us |    102.549 us |     297.512 us |     3,535.8 us |     4.86 |     0.41 |   257.8125 |  70.3125 |        - |   9.59 KB |
|      UploadBase64LoadSm |    12,283.4 us |    245.510 us |     680.306 us |    12,193.1 us |    16.94 |     0.93 |   125.0000 |  15.6250 |        - |   10.4 KB |
|      UploadBase64LoadMd |    83,318.0 us |  1,643.788 us |   1,892.989 us |    83,576.1 us |   114.92 |     2.56 |   125.0000 |        - |        - |  11.53 KB |
|      UploadBase64LoadLg |   830,375.3 us | 12,552.587 us |  10,481.978 us |   829,533.9 us | 1,145.29 |    14.20 |   500.0000 | 375.0000 | 375.0000 |  10.25 KB |
|      UploadBase64LoadXl | 2,770,966.2 us | 61,460.987 us | 171,328.666 us | 2,736,007.3 us | 3,821.83 |   235.19 |   875.0000 | 750.0000 | 625.0000 |   9.87 KB |
|        UploadFileLoadXs |     3,641.1 us |     89.774 us |     263.291 us |     3,616.5 us |     5.02 |     0.36 |   308.5938 |  97.6563 |        - |  10.95 KB |
|        UploadFileLoadSm |     4,108.4 us |    236.190 us |     681.463 us |     4,069.6 us |     5.67 |     0.94 |   281.2500 |  93.7500 |        - |   8.38 KB |
|        UploadFileLoadMd |     8,656.1 us |    246.595 us |     723.221 us |     8,622.2 us |    11.94 |     0.99 |   656.2500 | 250.0000 |        - |   8.38 KB |
|        UploadFileLoadLg |    32,786.0 us |    840.007 us |   2,423.612 us |    32,796.2 us |    45.22 |     3.33 |  3000.0000 | 375.0000 |        - |   8.99 KB |
|        UploadFileLoadXl |    87,284.9 us |  1,737.716 us |   3,886.652 us |    87,453.7 us |   120.39 |     5.32 | 10500.0000 | 437.5000 |        - |  10.91 KB |
|   UploadFileLoadMultiXs |       725.0 us |      2.071 us |       1.937 us |       724.8 us |     1.00 |     0.00 |    46.8750 |        - |        - |   6.14 KB |
|   UploadFileLoadMultiSm |       772.1 us |     11.915 us |      11.146 us |       767.9 us |     1.06 |     0.02 |    49.8047 |        - |        - |   6.14 KB |
|   UploadFileLoadMultiMd |     1,071.5 us |      4.423 us |       3.921 us |     1,072.2 us |     1.48 |     0.01 |    66.4063 |        - |        - |   6.14 KB |
|   UploadFileLoadMultiLg |     3,544.7 us |     11.093 us |      10.377 us |     3,547.9 us |     4.89 |     0.02 |   187.5000 |        - |        - |   6.14 KB |
|   UploadFileLoadMultiXl |    10,866.8 us |     47.557 us |      39.712 us |    10,869.8 us |    14.99 |     0.07 |   562.5000 |        - |        - |   6.14 KB |
|      UploadStreamLoadXs |     3,482.4 us |     89.663 us |     262.967 us |     3,462.6 us |     4.80 |     0.36 |   308.5938 | 105.4688 |        - |   8.54 KB |
|      UploadStreamLoadSm |     3,226.7 us |    230.324 us |     653.390 us |     3,004.1 us |     4.45 |     0.90 |   250.0000 |  62.5000 |        - |   9.67 KB |
|      UploadStreamLoadMd |     5,930.3 us |    287.410 us |     842.923 us |     5,860.9 us |     8.18 |     1.16 |   500.0000 | 156.2500 |        - |    9.5 KB |
|      UploadStreamLoadLg |    21,925.8 us |    631.662 us |   1,791.919 us |    22,074.7 us |    30.24 |     2.46 |  1718.7500 | 343.7500 |        - |  13.74 KB |
|      UploadStreamLoadXl |    64,374.5 us |  1,276.471 us |   3,537.095 us |    63,691.8 us |    88.79 |     4.86 |  5437.5000 | 375.0000 |        - |  13.98 KB |
| UploadStreamLoadMultiXs |       694.2 us |      1.433 us |       1.340 us |       694.5 us |     0.96 |     0.00 |    48.8281 |        - |        - |   6.14 KB |
| UploadStreamLoadMultiSm |       728.7 us |      2.624 us |       2.455 us |       728.1 us |     1.01 |     0.00 |    49.8047 |        - |        - |   6.14 KB |
| UploadStreamLoadMultiMd |       977.7 us |      5.329 us |       4.724 us |       977.8 us |     1.35 |     0.01 |    58.5938 |        - |        - |   6.14 KB |
| UploadStreamLoadMultiLg |     2,846.1 us |      8.120 us |       7.198 us |     2,848.1 us |     3.93 |     0.01 |   117.1875 |        - |        - |   6.14 KB |
| UploadStreamLoadMultiXl |     8,442.1 us |     25.867 us |      24.196 us |     8,444.9 us |    11.64 |     0.04 |   296.8750 |        - |        - |   6.14 KB |

**** Key
* UploadBase64Load* - 20x uploads, default json model binding, convert from base64 to byte[]
* UploadFileLoad* - 20x uploads, default `IFormFile` model binding
* UploadFileLoadMulti* - 1x upload, 20x files, default `IFormFile` model binding
* UploadStreamLoad* - 20x uploads, custom `StreamFiles` stream processing and model binding
* UploadStreamLoadMulti - 1x upload, 20x files, custom `StreamFiles` stream processing and model binding
