﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;

using BenchmarkDotNet.Running;

namespace File.Api.PerformanceTests {
    using System.IO;

    public class Benchmarks {

        public readonly static string BaseDir;
        public readonly static StringContent[] Base64Files;
        public readonly static StringContent[][] Base64LoadFiles;
        public readonly static MultipartFormDataContent[] Files;
        public readonly static MultipartFormDataContent[] CollectionFiles;
        public readonly static MultipartFormDataContent[][] LoadFiles;

        public const int LOAD_LIMIT = 20;

        static Benchmarks() {
            BaseDir = Path.Combine(Directory.GetCurrentDirectory(), "resources");

            var filenames = new[] { "xs.png", "sm.jpg", "md.jpg", "lg.jpg", "xl.exe", "xxl.exe" };

            Base64Files = filenames
                .Select(x => {
                    var path = Path.Combine(BaseDir, x);
                    var base64 = Convert.ToBase64String(File.ReadAllBytes(path));
                    return new StringContent($"{{\"name\":\"name\",\"description\":\"description\",\"base64\":\"{base64}\"}}", Encoding.UTF8, "application/json");
                })
                .ToArray();

            Base64LoadFiles = filenames
                .Where((x,i) => i<5)
                .Select(x => {
                    var data = new StringContent[LOAD_LIMIT];
                    for (int i = 0; i < LOAD_LIMIT; i++) {
                        var path = Path.Combine(BaseDir, x);
                        var base64 = Convert.ToBase64String(File.ReadAllBytes(path));
                        data[i] = new StringContent($"{{\"name\":\"name\",\"description\":\"description\",\"base64\":\"{base64}\"}}", Encoding.UTF8, "application/json");
                    }
                    return data;
                })
                .ToArray();

            Files = filenames
                .Select(x => {
                    var path = Path.Combine(BaseDir, x);
                    var multicontent = new MultipartFormDataContent();
                    ByteArrayContent bytes = new ByteArrayContent(File.ReadAllBytes(path));
                    bytes.Headers.Add("Content-Type", x.EndsWith(".png") ? "image/png" : x.EndsWith(".jpg") ? "image/jpeg" : "application/octet-stream");
                    multicontent.Add(bytes, "files", x);
                    multicontent.Add(new StringContent("name"), "name");
                    multicontent.Add(new StringContent("description"), "description");
                    return multicontent;
                })
                .ToArray();

            CollectionFiles = filenames
                .Select(x => {
                    var path = Path.Combine(BaseDir, x);
                    var multicontent = new MultipartFormDataContent();
                    ByteArrayContent bytes = new ByteArrayContent(File.ReadAllBytes(path));
                    bytes.Headers.Add("Content-Type", x.EndsWith(".png") ? "image/png" : x.EndsWith(".jpg") ? "image/jpeg" : "application/octet-stream");
                    for (int i = 0; i < LOAD_LIMIT; i++)
                        multicontent.Add(bytes, "files", x);
                    multicontent.Add(new StringContent("name"), "name");
                    multicontent.Add(new StringContent("description"), "description");
                    return multicontent;
                })
                .ToArray();

            LoadFiles = filenames
                .Where((x, i) => i < 5)
                .Select(x => {
                    var data = new MultipartFormDataContent[LOAD_LIMIT];
                    for (int i = 0; i < LOAD_LIMIT; i++) {
                        var path = Path.Combine(BaseDir, x);
                        var multicontent = new MultipartFormDataContent();
                        ByteArrayContent bytes = new ByteArrayContent(File.ReadAllBytes(path));
                        bytes.Headers.Add("Content-Type", x.EndsWith(".png") ? "image/png" : x.EndsWith(".jpg") ? "image/jpeg" : "application/octet-stream");
                        multicontent.Add(bytes, "files", x);
                        multicontent.Add(new StringContent("name"), "name");
                        multicontent.Add(new StringContent("description"), "description");
                        data[i] = multicontent;
                    }
                    return data;
                })
                .ToArray();

            // clean out last run test files
            var tmpDir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
            if (Directory.Exists(tmpDir))
                Directory.Delete(tmpDir, true);
            Directory.CreateDirectory(tmpDir);
        }


        public static void Main(string[] args) {
            //BenchmarkRunner.Run<DownloadBenchmarks>();
            //BenchmarkRunner.Run<UploadBenchmarks>();
            BenchmarkRunner.Run<UploadBenchmarksLoad>();
        }
    }
}
