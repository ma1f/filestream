﻿using System.Net.Http;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Exporters;

namespace File.Api.PerformanceTests {

    [JsonExporterAttribute.Brief]
    [MemoryDiagnoser]
    public class UploadBenchmarks {

        readonly TestServer _testServer;
        readonly HttpClient _client;
        
        public UploadBenchmarks() {
            var builder = new WebHostBuilder()
                   .UseStartup<Startup>();

            _testServer = new TestServer(builder);
            _client = _testServer.CreateClient();
        }

        [Benchmark]
        public string UploadBase64Xs() => _client.PostAsync("api/base64", Benchmarks.Base64Files[0]).Result.Content.ReadAsStringAsync().Result;

        [Benchmark]
        public void UploadBase64Sm() => _client.PostAsync("api/base64", Benchmarks.Base64Files[1]).Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void UploadBase64Md() => _client.PostAsync("api/base64", Benchmarks.Base64Files[2]).Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void UploadBase64Lg() => _client.PostAsync("api/base64", Benchmarks.Base64Files[3]).Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void UploadBase64Xl() => _client.PostAsync("api/base64", Benchmarks.Base64Files[4]).Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void UploadBase64Xxl() => _client.PostAsync("api/base64", Benchmarks.Base64Files[5]).Result.Content.ReadAsStringAsync().Wait();

        [Benchmark(Baseline = true)]
        public void UploadFileXs() => _client.PostAsync("api/upload", Benchmarks.Files[0]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadFileSm() => _client.PostAsync("api/upload", Benchmarks.Files[1]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadFileMd() => _client.PostAsync("api/upload", Benchmarks.Files[2]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadFileLg() => _client.PostAsync("api/upload", Benchmarks.Files[3]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadFileXl() => _client.PostAsync("api/upload", Benchmarks.Files[4]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadFileXxl() => _client.PostAsync("api/upload", Benchmarks.Files[5]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamXs() => _client.PostAsync("api/stream", Benchmarks.Files[0]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamSm() => _client.PostAsync("api/stream", Benchmarks.Files[1]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamMd() => _client.PostAsync("api/stream", Benchmarks.Files[2]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamLg() => _client.PostAsync("api/stream", Benchmarks.Files[3]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamXl() => _client.PostAsync("api/stream", Benchmarks.Files[4]).Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void UploadStreamXxl() => _client.PostAsync("api/stream", Benchmarks.Files[5]).Result.Content.LoadIntoBufferAsync().Wait();
    }
}
