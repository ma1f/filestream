﻿using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Exporters;

namespace File.Api.PerformanceTests {

    [JsonExporterAttribute.Brief]
    [MemoryDiagnoser]
    public class UploadBenchmarksLoad {

        readonly TestServer _testServer;
        readonly HttpClient _client;

        public UploadBenchmarksLoad() {
            var builder = new WebHostBuilder()
                   .UseStartup<Startup>();

            _testServer = new TestServer(builder);
            _client = _testServer.CreateClient();
        }

        [Benchmark]
        public void UploadBase64LoadXs() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/base64", Benchmarks.Base64LoadFiles[0][i]).Wait());

        [Benchmark]
        public void UploadBase64LoadSm() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/base64", Benchmarks.Base64LoadFiles[1][i]).Wait());

        [Benchmark]
        public void UploadBase64LoadMd() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/base64", Benchmarks.Base64LoadFiles[2][i]).Wait());

        [Benchmark]
        public void UploadBase64LoadLg() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/base64", Benchmarks.Base64LoadFiles[3][i]).Wait());

        [Benchmark]
        public void UploadBase64LoadXl() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/base64", Benchmarks.Base64LoadFiles[4][i]).Wait());

        [Benchmark]
        public void UploadFileLoadXs() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/upload", Benchmarks.LoadFiles[0][i]).Wait());

        [Benchmark]
        public void UploadFileLoadSm() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/upload", Benchmarks.LoadFiles[1][i]).Wait());

        [Benchmark]
        public void UploadFileLoadMd() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/upload", Benchmarks.LoadFiles[2][i]).Wait());

        [Benchmark]
        public void UploadFileLoadLg() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/upload", Benchmarks.LoadFiles[3][i]).Wait());

        [Benchmark]
        public void UploadFileLoadXl() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/upload", Benchmarks.LoadFiles[4][i]).Wait());

        [Benchmark(Baseline = true)]
        public void UploadFileLoadMultiXs() => _client.PostAsync("api/upload", Benchmarks.CollectionFiles[0]).Wait();

        [Benchmark]
        public void UploadFileLoadMultiSm() => _client.PostAsync("api/upload", Benchmarks.CollectionFiles[1]).Wait();

        [Benchmark]
        public void UploadFileLoadMultiMd() => _client.PostAsync("api/upload", Benchmarks.CollectionFiles[2]).Wait();

        [Benchmark]
        public void UploadFileLoadMultiLg() => _client.PostAsync("api/upload", Benchmarks.CollectionFiles[3]).Wait();

        [Benchmark]
        public void UploadFileLoadMultiXl() => _client.PostAsync("api/upload", Benchmarks.CollectionFiles[4]).Wait();

        [Benchmark]
        public void UploadStreamLoadXs() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/stream", Benchmarks.LoadFiles[0][i]).Wait());

        [Benchmark]
        public void UploadStreamLoadSm() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/stream", Benchmarks.LoadFiles[1][i]).Wait());

        [Benchmark]
        public void UploadStreamLoadMd() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/stream", Benchmarks.LoadFiles[2][i]).Wait());

        [Benchmark]
        public void UploadStreamLoadLg() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/stream", Benchmarks.LoadFiles[3][i]).Wait());

        [Benchmark]
        public void UploadStreamLoadXl() => Parallel.For(0, Benchmarks.LOAD_LIMIT, (i, s) => _client.PostAsync("api/stream", Benchmarks.LoadFiles[4][i]).Wait());

        [Benchmark]
        public void UploadStreamLoadMultiXs() => _client.PostAsync("api/stream", Benchmarks.CollectionFiles[0]).Wait();

        [Benchmark]
        public void UploadStreamLoadMultiSm() => _client.PostAsync("api/stream", Benchmarks.CollectionFiles[1]).Wait();

        [Benchmark]
        public void UploadStreamLoadMultiMd() => _client.PostAsync("api/stream", Benchmarks.CollectionFiles[2]).Wait();

        [Benchmark]
        public void UploadStreamLoadMultiLg() => _client.PostAsync("api/stream", Benchmarks.CollectionFiles[3]).Wait();

        [Benchmark]
        public void UploadStreamLoadMultiXl() => _client.PostAsync("api/stream", Benchmarks.CollectionFiles[4]).Wait();
    }
}
