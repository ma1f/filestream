﻿using System.Net.Http;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Exporters;

namespace File.Api.PerformanceTests {

    [JsonExporterAttribute.Brief]
    [MemoryDiagnoser]
    public class DownloadBenchmarks {

        readonly TestServer _testServer;
        readonly HttpClient _client;
        
        public DownloadBenchmarks() {
            var builder = new WebHostBuilder()
                   .UseStartup<Startup>();

            _testServer = new TestServer(builder);
            _client = _testServer.CreateClient();
        }

        [Benchmark(Baseline = true)]
        public void Ping() => _client.GetAsync("api/ping").Result.Content.ReadAsStringAsync().Wait();
        
        [Benchmark]
        public void DownloadBase64Xs() => _client.GetAsync("api/base64/0").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64Sm() => _client.GetAsync("api/base64/1").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64Md() => _client.GetAsync("api/base64/2").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64Lg() => _client.GetAsync("api/base64/3").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64Xl() => _client.GetAsync("api/base64/4").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64Xxl() => _client.GetAsync("api/base64/5").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemoryXs() => _client.GetAsync("api/base64/0/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemorySm() => _client.GetAsync("api/base64/1/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemoryMd() => _client.GetAsync("api/base64/2/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemoryLg() => _client.GetAsync("api/base64/3/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemoryXl() => _client.GetAsync("api/base64/4/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadBase64MemoryXxl() => _client.GetAsync("api/base64/5/memory").Result.Content.ReadAsStringAsync().Wait();

        [Benchmark]
        public void DownloadFileXs() => _client.GetAsync("api/file/0").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileSm() => _client.GetAsync("api/file/1").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMd() => _client.GetAsync("api/file/2").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileLg() => _client.GetAsync("api/file/3").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileXl() => _client.GetAsync("api/file/4").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileXxl() => _client.GetAsync("api/file/5").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemoryXs() => _client.GetAsync("api/file/0/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemorySm() => _client.GetAsync("api/file/1/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemoryMd() => _client.GetAsync("api/file/2/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemoryLg() => _client.GetAsync("api/file/3/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemoryXl() => _client.GetAsync("api/file/4/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileMemoryXxl() => _client.GetAsync("api/file/5/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamXs() => _client.GetAsync("api/file/0/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamSm() => _client.GetAsync("api/file/1/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamMd() => _client.GetAsync("api/file/2/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamLg() => _client.GetAsync("api/file/3/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamXl() => _client.GetAsync("api/file/4/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadFileFileStreamXxl() => _client.GetAsync("api/file/5/filestream").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamXs() => _client.GetAsync("api/filestream/0").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamSm() => _client.GetAsync("api/filestream/1").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMd() => _client.GetAsync("api/filestream/2").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamLg() => _client.GetAsync("api/filestream/3").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamXl() => _client.GetAsync("api/filestream/4").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamXxl() => _client.GetAsync("api/filestream/5").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemoryXs() => _client.GetAsync("api/filestream/0/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemorySm() => _client.GetAsync("api/filestream/1/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemoryMd() => _client.GetAsync("api/filestream/2/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemoryLg() => _client.GetAsync("api/filestream/3/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemoryXl() => _client.GetAsync("api/filestream/4/memory").Result.Content.LoadIntoBufferAsync().Wait();

        [Benchmark]
        public void DownloadStreamMemoryXxl() => _client.GetAsync("api/filestream/5/memory").Result.Content.LoadIntoBufferAsync().Wait();
    }
}
